#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "main.h"
#include "queue.h"
#include "resigned.h"
#include "condv.h"
#include "sem_mutex.h"

pthread_mutex_t waiting_room = PTHREAD_MUTEX_INITIALIZER; // Mutex na zmiany w poczekalni

int chairs = 10;
volatile int current_in = -1;
volatile unsigned int current_waiting = 0;
unsigned int client_counter = 1; // Zmienna do przydzielania numerków
int debug = 0;
int zmienne = 0;

int main(int argc, char **argv) {

    if(argc > 1) {
        int i;
        for(i = 1; i < argc; i++) {
            if(strcmp(argv[ i ], "-h") == 0) {
                printf("Uzycie:\n\t%s [liczba_krzesel] [-debug] [-zmienne]\n", argv[ 0 ]);
                exit(EXIT_SUCCESS);
            } else if(strcmp(argv[ i ], "-debug") == 0) {
                debug = 1;
            } else if(strcmp(argv[ i ], "-zmienne") == 0) {
                zmienne = 1;
            } else {
                chairs = atoi(argv[ i ]);
                if( chairs <= 0 ) {
                    fprintf(stderr, "Błędna liczba krzeseł lub nieznany parametr\n");
                    exit(EXIT_SUCCESS);
                }
            }
        }
    }

    if( zmienne ) {
        condv_main();
    } else { // Semafory i mutexy
        mutex_semaphore_main();
    }

    exit(EXIT_SUCCESS);
}

void logger() {
    printf("Res:%d WRomm: %d/%d [in: ", get_resigned(), current_waiting, chairs);
    if( current_in != -1 )
        printf("%d]\n", current_in);
    else
        printf("empty]\n");

    if( debug ) {
        PrintQueue();
        print_resigned();
    }
}

long random_as_possible(long max) { // Funkcja generująca możliwie najbardziej losową liczbę z przedziału [0,max]
    unsigned long
            num_bins = (unsigned long) max + 1,
            num_rand = (unsigned long) RAND_MAX + 1,
            bin_size = num_rand / num_bins,
            defect   = num_rand % num_bins;

    long x;
    do {
        x = random();
    } while (num_rand - defect <= (unsigned long)x);

    return x/bin_size;
}
