#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#include "main.h"
#include "sem_mutex.h"
#include "resigned.h"
#include "queue.h"

// MUTEX/SEMAPHORE BEG
sem_t customers; // Semafor informujący o klientach w poczekalni
sem_t chair; // Semafor informujący o wolnym fotelu do strzyżenia
sem_t sit; // Semafor informujący że klient usiadł na fotelu
sem_t end; // Semafor informujący o zakończeniu strzyżenia
// MUTEX/SEMAPHORE END

void mutex_semaphore_main() {
    pthread_t barber_t;
    int iret;

    iret = pthread_create(&barber_t, NULL, barber_semmutex, NULL);
    if( iret ) {
        fprintf( stderr, "Error - pthread_create(barber_t) return code: %d\n", iret );
        exit(EXIT_FAILURE);
    }

    // Początkowe ustawienie semaforów
    sem_init(&customers, 0, 0); // Brak klientów
    sem_init(&chair, 0, 0); // Fotel jest nieprzygotowany
    sem_init(&end, 0, 0); // Strzyżenie nie jest zakończone
    sem_init(&sit, 0, 0); // Klient jeszcze nie usiadł na fotelu

    srand(time(NULL));

    while( 1 ) {
        pthread_t customer_t; // Zmienna automatyczna do uruchomienia wątku
        int *number = malloc(sizeof(int)); // Numerek do przekazania klientowi
        *number = client_counter;
        iret = pthread_create(&customer_t, NULL, customer_semmutex, (void *) number);
        if( iret ) {
            fprintf( stderr, "Error - pthread_create(customer_t, i:%d) return code: %d\n", client_counter, iret );
            exit(EXIT_FAILURE);
        }
        ++client_counter;
        usleep(1000 * random_as_possible(4)* 250); // Sleep przed następnym wejściem klienta
    }
}

void *barber_semmutex(void *ptr) {
    while(1) {
        sem_post(&chair); // Fotel gotowy na przyjęcie klienta
        sem_wait(&customers); // Czekanie na klienta
        sem_wait(&sit); // Czekanie aż klient usiądzie
        logger(); // Klient siedzi na fotelu wypisanie logu
        int time_to_sleep = 1000*random_as_possible(4)*400; // Czas strzyżenia
        usleep(time_to_sleep); // Strzyżenie
        sem_post(&end); // Strzyżenie zakończone klient może wyjść
        current_in = -1; // Aktualnie nikt nie siedzi na fotelu
        if( current_waiting == 0) // Jeśli brak klientów to wypisujemy informacje na ekran
            logger();
    }
}

void *customer_semmutex(void *ptr) {
    int number = *((int *) ptr); // Odebranie numerka
    free( ptr );
    pthread_mutex_lock(&waiting_room); // Mutex sekcja krytyczna poczekalnia, dodawanie do poczekalni
    if(current_waiting >= chairs) { // Jeżel nie ma miejsc to dodanie się do listy rezygnacji
        add_resigned(number);
        logger();
        pthread_mutex_unlock(&waiting_room); // Odblokowanie mutexa na sekcję krytyczną
        pthread_exit(NULL);
    }
    ++current_waiting; // Inkrementacja licznika czekających
    Enqueue(number); // Wejście do kolejki klientów
    logger();
    pthread_mutex_unlock(&waiting_room); // Odblokowanie mutexa na sekcję krytyczną

    sem_post(&customers); // Informowanie fryzjera o czekającym kliencie
    while(1) { // Czekanie na fotel w pętli upewniającej się o kolei
        sem_wait(&chair); // Czekanie na otwarcie semafora
        if(FrontQueue() == number) // Jeżeli kolej tego wątku to wyjście z pętli
            break;
        sem_post(&chair); // Jeżeli to nie kolej tego wątku to podniesienie opuszczonego semafora
    }

    pthread_mutex_lock(&waiting_room); // Mutex sekcja krytyczna poczekalnia, zmiany w poczekalni przy wyjściu
    current_in = (unsigned int) number;
    --current_waiting;
    Dequeue(); // Wyjście z kolejki
    pthread_mutex_unlock(&waiting_room); // Odblokowanie sekcji krytycznej poczekalnia

    sem_post(&sit); // Informowanie fryzjera o zakończeniu procedury siadania na fotel
    sem_wait(&end); // Czekanie na koniec strzyżenia

    pthread_exit(NULL); // Koniec wątku
}
