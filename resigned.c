#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "resigned.h"
#include "main.h"

pthread_mutex_t resigned_mutex = PTHREAD_MUTEX_INITIALIZER; // Mutex na zmiany w zrezygnowanych klientach
int *resigned = NULL;
volatile unsigned int current_resigned = 0;

void add_resigned(int num) {
    pthread_mutex_lock(&resigned_mutex);
    ++current_resigned;
    if( resigned == NULL )
        resigned = malloc(sizeof(int));
    else
        resigned = realloc(resigned, sizeof(int)*current_resigned);
    resigned[current_resigned - 1] = num;
    pthread_mutex_unlock(&resigned_mutex);
}

void print_resigned() {
    pthread_mutex_lock(&resigned_mutex);
    int i;
    printf("Resigned: ");
    if( current_resigned == 0 )
        printf("empty");
    for(i = 0; i < current_resigned; i++) {
        printf("%d ", resigned[ i ]);
    }
    printf("\n");
    pthread_mutex_unlock(&resigned_mutex);
}

unsigned int get_resigned() {
    return current_resigned;
}
