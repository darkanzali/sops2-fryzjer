#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "queue.h"
#include "main.h"

pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER; // Mutex na operacje na kolejce
Queue *queue; // Kolejka czekających klientów

void Enqueue(int val) {
    pthread_mutex_lock(&queue_mutex);
    Queue *tmp;
    if(queue == NULL) {
        queue = malloc(sizeof(Queue));
        queue -> next = NULL;
        queue -> value = val;
    } else {
        tmp = queue;
        while(tmp -> next != NULL)
            tmp = tmp ->next;
        Queue *new_item;
        new_item = malloc(sizeof(Queue));
        new_item -> value = val;
        new_item -> next = NULL;
        tmp -> next = new_item;
    }
    pthread_mutex_unlock(&queue_mutex);
}

Queue *EnqueueTicket(int val) {
    pthread_mutex_lock(&queue_mutex);
    Queue *tmp, *newT;
    if(queue == NULL) {
        queue = malloc(sizeof(Queue));
        queue -> next = NULL;
        queue -> value = val;
        int ret;
        ret = pthread_mutex_init(&(queue -> ticket_mutex), NULL);
        if( ret ) {
            fprintf( stderr, "Error - pthread_mutex_init() return code: %d\n", ret );
            exit(EXIT_FAILURE);
        }
        ret = pthread_cond_init(&(queue -> ticket_cond), NULL);
        if( ret ) {
            fprintf( stderr, "Error - pthread_cond_init() return code: %d\n", ret );
            exit(EXIT_FAILURE);
        }
        newT = queue;
    } else {
        tmp = queue;
        while(tmp -> next != NULL)
            tmp = tmp ->next;
        Queue *new_item;
        new_item = malloc(sizeof(Queue));
        new_item -> value = val;
        new_item -> next = NULL;
        int ret;
        ret = pthread_mutex_init(&(new_item -> ticket_mutex), NULL);
        if( ret ) {
            fprintf( stderr, "Error - pthread_mutex_init() return code: %d\n", ret );
            exit(EXIT_FAILURE);
        }
        ret = pthread_cond_init(&(new_item -> ticket_cond), NULL);
        if( ret ) {
            fprintf( stderr, "Error - pthread_cond_init() return code: %d\n", ret );
            exit(EXIT_FAILURE);
        }
        tmp -> next = new_item;
        newT = new_item;
    }
    pthread_mutex_unlock(&queue_mutex);
    return newT;
}

void Dequeue() {
    pthread_mutex_lock(&queue_mutex);
    Queue *new_first;
    new_first = queue -> next;
    free(queue);
    queue = new_first;
    pthread_mutex_unlock(&queue_mutex);
}

void DequeueTicket() {
    pthread_mutex_lock(&queue_mutex);
    Queue *new_first;
    new_first = queue -> next;
    pthread_mutex_destroy(&(queue -> ticket_mutex));
    pthread_cond_destroy(&(queue -> ticket_cond));
    free(queue);
    queue = new_first;
    pthread_mutex_unlock(&queue_mutex);
}

int FrontQueue() {
    return queue -> value;
}

Queue *FrontQueueTicket() {
    return queue;
}

void PrintQueue() {
    pthread_mutex_lock(&queue_mutex);
    Queue *tmp;
    tmp = queue;
    printf("Queue: ");
    if( tmp == NULL )
        printf("empty");
    while(tmp != NULL) {
        printf("%d ", tmp -> value);
        tmp = tmp -> next;
    }
    printf("\n");
    pthread_mutex_unlock(&queue_mutex);
}
