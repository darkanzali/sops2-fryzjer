#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include "main.h"
#include "condv.h"
#include "resigned.h"
#include "queue.h"

pthread_mutex_t haircutMutex = PTHREAD_MUTEX_INITIALIZER; // Strzyżenie

pthread_cond_t customersCondv = PTHREAD_COND_INITIALIZER; // Informacja o czekających klientach
pthread_cond_t haircutEndCondv = PTHREAD_COND_INITIALIZER; // Koniec strzyżenia
pthread_cond_t customerReadyCondv = PTHREAD_COND_INITIALIZER; // Klient siedzi na fotelu i czeka na strzyżenie

volatile int sleeping = 0;
volatile int current_turn = -1;

void condv_main() {
    pthread_t barber_t;
    int iret;

    iret = pthread_create(&barber_t, NULL, barber_condv, NULL);
    if( iret ) {
        fprintf( stderr, "Error - pthread_create(barber_t) return code: %d\n", iret );
        exit(EXIT_FAILURE);
    }

    srand(time(NULL));

    while( 1 ) {
        pthread_t customer_t; // Zmienna automatyczna do uruchomienia wątku
        int *number = malloc(sizeof(int)); // Numerek do przekazania klientowi
        *number = client_counter;
        iret = pthread_create(&customer_t, NULL, customer_condv, (void *) number);
        if( iret ) {
            fprintf( stderr, "Error - pthread_create(customer_t, i:%d) return code: %d\n", client_counter, iret );
            exit(EXIT_FAILURE);
        }
        ++client_counter;
        usleep(1000 * random_as_possible(4)*250); // Sleep przed następnym wejściem klienta
    }
}

void *barber_condv(void *ptr) {
    while(1) {
        // Sprawdzanie poczekalni BEG
        pthread_mutex_lock(&waiting_room); // Mutex na sprawdzanie poczekalni
        while(current_waiting == 0) { // Jeśli brak klientów to spanie
            sleeping = 1; // Ustawianie informacji o zaśnięciu fryzjera
            logger(); // Wypisanie loga o braku klientów
            pthread_cond_wait(&customersCondv, &waiting_room); // Czekanie na informację że przyszedł klient
            sleeping = 0; // Kasujemy informację o spaniu
        }
        pthread_mutex_unlock(&waiting_room); // Opuszczenie sekcji krytycznej
        // Sprawdzanie poczekalni END

        Queue *current = FrontQueueTicket(); // Bierzemy bilet z kolejki do obudzenia pierwszego klienta
        pthread_mutex_lock(&(current -> ticket_mutex)); // Blokujemy mutex klienta do obudzenia dla pewności
        current_turn = current->value; // Ustawianie informacji czyja kolej
        pthread_cond_signal(&(current -> ticket_cond)); // Budzimy klienta który stoi na początku kolejki

        // Strzyżenie BEG
        pthread_mutex_lock(&haircutMutex); // Procedura strzyżenia rozpoczęta
        pthread_mutex_unlock(&(current -> ticket_mutex)); // Odblokowanie drogi klientowi którego jest kolej
        pthread_cond_wait(&customerReadyCondv, &haircutMutex); // Czekanie na gotowość klienta

        long time_to_sleep = 1000*random_as_possible(4)*400; // Czas strzyżenia
        usleep(time_to_sleep); // Strzyżenie

        pthread_cond_broadcast(&haircutEndCondv); // Informowanie klienta o zakończeniu strzyżenia
        // Strzyżenie END
        current_in = -1; // Aktualnie brak kogokolwiek na fotelu
        pthread_mutex_unlock(&haircutMutex); // Odblokowanie mutexa od strzyżenia
    }
}

void *customer_condv(void *ptr) {
    int number = *((int *) ptr); // Odebranie numerka
    free( ptr );
    pthread_mutex_lock(&waiting_room); // Mutex na sprawdzanie czy są miejsca w poczekalni
    if(current_waiting >= chairs) { // Jeżel nie to dodanie do listy rezygnacji
        add_resigned(number);
        logger();
        pthread_mutex_unlock(&waiting_room); // Odblokowanie mutexa na poczekalnię
        pthread_exit(NULL);
    }
    ++current_waiting; // Inkrementacja liczby czekających
    Queue *me = EnqueueTicket(number); // Wejście do kolejki klientów
    logger();
    if( sleeping ) // Jeżeli fryzjer śpi to budzenie go
        pthread_cond_signal(&customersCondv);

    pthread_mutex_unlock(&waiting_room); // Odblokowanie mutexa na zmiany w poczekalni

    // Czekanie na fotel BEG
    pthread_mutex_lock(&(me -> ticket_mutex)); // Blokownie mutexa do zmiennej warunkowej
    while(current_turn != number) // Pętla upewniająca się że kolej tego wątku
        pthread_cond_wait(&(me -> ticket_cond), &(me -> ticket_mutex)); // Czekanie na zmienną warunkową, na swoją kolej

    pthread_mutex_unlock(&(me -> ticket_mutex)); // Odblokowanie mutexa na czekanie
    // Czekanie na fotel END

    // Strzyżenie BEG
    pthread_mutex_lock(&waiting_room); // Wychodzenie z poczekalni
    current_in = (unsigned int) number;
    --current_waiting;
    DequeueTicket(); // Zdjęcie siebie z kolejki
    logger();
    pthread_mutex_unlock(&waiting_room); // Odblokowanie poczekalni

    pthread_mutex_lock(&haircutMutex); // Mutex na strzyżenie
    pthread_cond_broadcast(&customerReadyCondv); // Informacja że klient jest gotowy, budzenie fryzjera
    pthread_cond_wait(&haircutEndCondv, &haircutMutex); // Czekanie na zmiennej warunkowej na zakończenie strzyżenia
    pthread_mutex_unlock(&haircutMutex); // Odblokowanie mutexa na strzyżenie
    // Strzyżenie END

    pthread_exit(NULL); // Kończenie wątku
}
