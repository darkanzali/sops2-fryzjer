#ifndef QUEUE_H
#define QUEUE_H

typedef struct queue {
    int value;
    pthread_mutex_t ticket_mutex;
    pthread_cond_t ticket_cond;
    struct queue *next;
} Queue;

void Enqueue(int val);
Queue *EnqueueTicket(int val);
void Dequeue();
void DequeueTicket();
int FrontQueue();
Queue *FrontQueueTicket();
void PrintQueue();

#endif //QUEUE_H
