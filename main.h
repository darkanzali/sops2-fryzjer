#ifndef MAIN_H
#define MAIN_H

void logger();
long random_as_possible(long max);

extern int chairs;
extern volatile int current_in;
extern volatile unsigned int current_waiting;
extern unsigned int client_counter;
extern pthread_mutex_t waiting_room;

#endif //MAIN_H
