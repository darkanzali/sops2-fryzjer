#ifndef SEM_MUTEX_H
#define SEM_MUTEX_H

void mutex_semaphore_main();
void *barber_semmutex(void *ptr);
void *customer_semmutex(void *ptr);

#endif //SEM_MUTEX_H
